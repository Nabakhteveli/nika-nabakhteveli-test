// Select's values
let electric = document.getElementById("electric-guitar").value;
let bass = document.getElementById("bass-guitar").value;
let acoustic = document.getElementById("acoustic-guitar").value;
let classical = document.getElementById("classical-guitar").value;
let strings = document.getElementById("strings").value;
let caposAndSlides = document.getElementById("caposnslides").value;
let picks = document.getElementById("picks").value;
let guitarCase = document.getElementById("case").value;
let disabledValue = document.getElementById("guitars").value;

// Input values
let info1 = document.getElementById("info1");
let info2 = document.getElementById("info2");
let info3 = document.getElementById("info3");
let info4 = document.getElementById("info4");
let info5 = document.getElementById("info5");


// This function changes values of the input's placeholder, depending on the other selection input, that user will choose
function changingFunction(){
    var x = document.getElementById("mySelect").value;
    if(x == electric || x == bass || x ==  acoustic || x == classical){
        document.getElementById("inputDiv").style.visibility = "visible";
        document.getElementById("inputDiv").style.marginTop = "-40px";
        document.getElementById("textarea").style.marginTop = "-150px";
    }else if(x == strings || x == caposAndSlides || x== picks || x== guitarCase){
        let input =  document.getElementById("info1-1").innerHTML = "Enter your product's dimensions";
        let textArea = document.getElementById("textarea");
        document.getElementById("inputDiv").style.visibility = "hidden";
        document.getElementById("info1-1").style.visibility = "visible";
        document.getElementById("textarea").style.visibility = "visible";
        document.getElementById("textarea").style.marginTop = "-158px";
    }
}

// This functions are for buttons.
//* This one "saves" information that user has entered
function saveAlert(){
    alert("Your Product Has Been Succesfully Added!");
}

//* An this one is for button, that resets the page and deletes all the information that user has entered
function resetText(){
    location.reload();    
}
